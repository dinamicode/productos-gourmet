var lista = new Vue({
  el: '#lista',
  data: {
    prods : [
			{
				name: 'Aguacate',
        img: 'imgs/lista/aguacate.jpg'
			},
			{
				name: 'Ajo',
        img: 'imgs/lista/ajo.jpg'
			},
      {
				name: 'Cebolla',
        img: 'imgs/lista/cebolla.jpg'
			},
      {
				name: 'Champiñon',
        img: 'imgs/lista/champi.jpg'
			},
      {
				name: 'Chile (variedades)',
        img: 'imgs/lista/chile.jpg'
			},
      {
				name: 'Fresa',
        img: 'imgs/lista/fresa.jpg'
			},
      {
				name: 'Frijol',
        img: 'imgs/lista/frijol.jpg'
			},
      {
				name: 'Jitomate',
        img: 'imgs/lista/jitomate.jpg'
			},
      {
				name: 'Lechuga',
        img: 'imgs/lista/lechuga.jpg'
			},
      {
				name: 'Limón',
        img: 'imgs/lista/limon.jpg'
			},
      {
				name: 'Maíz',
        img: 'imgs/lista/maiz.jpg'
			},
      {
				name: 'Mandarina',
        img: 'imgs/lista/mandarina.jpg'
			},
      {
				name: 'Mango',
        img: 'imgs/lista/mango.jpg'
			},
      {
				name: 'Manzana',
        img: 'imgs/lista/manzana.jpg'
			},
      {
				name: 'Naranja',
        img: 'imgs/lista/naranja.jpg'
			},
      {
				name: 'Papa',
        img: 'imgs/lista/papa.jpg'
			},
      {
				name: 'Platano',
        img: 'imgs/lista/platano.jpg'
			},
      {
				name: 'Sandía',
        img: 'imgs/lista/sandia.jpg'
			},
      {
				name: 'Uvas',
        img: 'imgs/lista/uvas.jpg'
			},
      {
				name: 'Zanahorias',
        img: 'imgs/lista/zanahoria.jpg'
			}
		]
  }
})
